import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Data } from './Data';
import {Router} from '@angular/router';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

    title = 'app';
    data: Data[];
    url = 'http://localhost:4000/results';
    year = [];
    count = [];

    public lineChartData: Array<any> = [
        {data: this.count, label: 'Python Language'},
    ];
    public lineChartLabels: Array<any> = this.year;
    public lineChartOptions: any = {
        responsive: true
    };
    public lineChartColors: Array<any> = [
        {
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'red',
            pointBorderColor: 'red',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];
    public lineChartLegend: Boolean = true;
    public lineChartType: String = 'line';

    constructor(private httpClient: HttpClient) {}

    ngOnInit() {
        this.httpClient.get(this.url).subscribe((res: Data[]) => {
            this.data = res.filter(r => {
                return r.name === 'Python';
            });
            this.data.forEach(y => {
                this.year.push(y.id);
                this.count.push(y.name);
            });
        });
    }

}
