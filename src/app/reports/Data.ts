export interface Data {
    id: Number;
    name: String;
    alterEgo: String;
}
